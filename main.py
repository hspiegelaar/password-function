def make_secret(length=12):

	from string import ascii_lowercase, ascii_uppercase, digits
	from random import randint

	mix = max([int(length/3), 1])
	allowed_chars = (ascii_lowercase + ascii_uppercase + digits) * mix + '!#$%&()*+,-.:;<=>?[]^_{|}~'

	secret = ''
	for _ in range(length):
	  secret += allowed_chars[randint(0, len(allowed_chars) - 1)]

	return secret


for i in range(20):
	print(make_secret(3))
	print(make_secret(20))
